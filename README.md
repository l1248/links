# Discover my school projects ! 📁

## Presentation

During my school year in La Rochelle Univerity, I managed to create some projects with diverse technologies such as Symfony, React, Wordpress and more...

## See my projects

To discover what I've done during this year, you can click the link below 😁.

➡️ [My projects](https://gitlab.univ-lr.fr/dashboard/groups)

## Copyright

© Benjamin PELAUDEIX - 2021 - All rights reserved
